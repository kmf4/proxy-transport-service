### перед запуском проекта необходимо скачать файл из проекта docker-local и положить его в корень проекта, также надо поставить docker-compose и желательно иметь unix систему для локальной разработки. 

## стуктура проекта:

```
workdir kmf
./proxy-transport-service
./docker-compose.yml
```

### следующим шагом надо поднять проект

```
    docker-compose up --build .
```

### как только поднимется сервис, для тестирования можно использовать следующие запросы

```
curl --location --request GET 'localhost:8902/kmf/call-api?url=https://amazon.com' \
--header 'Authorization: Bearer Basic asdjasdnasdj='
```

### для просмотра кеша

```
curl --location --request GET 'localhost:8902/kmf/cache'
```
