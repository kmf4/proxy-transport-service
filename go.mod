module kmf/proxy-transport-service

go 1.20

require (
	github.com/gorilla/mux v1.8.0
	github.com/segmentio/ksuid v1.0.4
	go.uber.org/zap v1.24.0
)

require (
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
)
