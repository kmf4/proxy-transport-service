package domain

type Response struct {
	Token  string `json:"token,omitempty"`
	ID     string `json:"id,omitempty"`
	Status int    `json:"status,omitempty"`
	Length int64  `json:"length,omitempty"`
}

type Request struct {
	Token  string `json:"token,omitempty"`
	Method string `json:"method,omitempty"`
	URL    string `json:"url,omitempty"`
}
