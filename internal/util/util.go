package util

import (
	"encoding/json"
	"errors"
	"net/http"
	"strings"
)

func ResponseOk(w http.ResponseWriter, body interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	_ = json.NewEncoder(w).Encode(body)
}

func ResponseError(w http.ResponseWriter, code int, body interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	_ = json.NewEncoder(w).Encode(body)
}

func GetTokenFromHeader(r *http.Request) (string, error) {
	splitToken := strings.Split(r.Header.Get("Authorization"), "Bearer")
	if len(splitToken) != 2 {
		return "", errors.New("token_not_found")
	}
	reqToken := strings.TrimSpace(splitToken[1])
	return reqToken, nil
}
