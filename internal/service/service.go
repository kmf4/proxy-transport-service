package service

import (
	"context"
	"kmf/proxy-transport-service/pkg/domain"
)

type Service struct {
}

func NewService() *Service {
	return &Service{}
}

func (s *Service) Collect(ctx context.Context, request domain.Request) {

}
