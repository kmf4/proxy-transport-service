package server

import (
	"context"
	"github.com/gorilla/mux"
	"kmf/proxy-transport-service/internal/handlers"
	"kmf/proxy-transport-service/pkg/logger"
	"net/http"
)

type Server struct {
	router  *mux.Router
	handler *handlers.Handler
}

func NewServer(handler *handlers.Handler) *Server {
	return &Server{router: mux.NewRouter(), handler: handler}
}

func (s *Server) Run(ctx context.Context) {
	s.router.HandleFunc("/kmf/call-api", s.handler.CallApi).Methods("GET")
	s.router.HandleFunc("/kmf/cache", s.handler.Cache).Methods("GET")

	logger.Info("Running on port...")
	logger.Fatal(http.ListenAndServe(":3000", s.router))
}
