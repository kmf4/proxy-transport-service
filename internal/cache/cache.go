package cache

import (
	"bytes"
	"fmt"
	"kmf/proxy-transport-service/pkg/domain"
	"sync"
)

type Cache struct {
	mu        sync.RWMutex
	requests  map[string]*domain.Request
	responses map[string]*domain.Response
}

func NewCache() *Cache {
	return &Cache{
		requests:  make(map[string]*domain.Request, 10),
		responses: make(map[string]*domain.Response, 10),
	}
}

func (c *Cache) SetHTTPRequest(req *domain.Request) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.requests[req.Token] = req
}

func (c *Cache) SetHTTPResponse(res *domain.Response) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.responses[res.Token] = res
}

func (c *Cache) String() string {
	var res bytes.Buffer
	for _, v := range c.responses {
		res.WriteString(fmt.Sprintf("%v\n", v))
	}

	res.WriteString("--")

	for _, v := range c.requests {
		res.WriteString(fmt.Sprintf("%v\n", v))
	}

	return res.String()
}
