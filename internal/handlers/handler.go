package handlers

import (
	"github.com/segmentio/ksuid"
	"io/ioutil"
	"kmf/proxy-transport-service/internal/cache"
	"kmf/proxy-transport-service/internal/service"
	"kmf/proxy-transport-service/internal/util"
	"kmf/proxy-transport-service/pkg/domain"
	"kmf/proxy-transport-service/pkg/logger"
	"net/http"
	"net/http/httputil"
	"net/url"
	"time"
)

type Handler struct {
	service *service.Service
	cache   *cache.Cache
}

func NewHandler(service *service.Service, cache *cache.Cache) *Handler {
	return &Handler{service: service, cache: cache}
}

type responseWriter struct {
	http.ResponseWriter
	contentLength int
	statusCode    int
}

func (rw *responseWriter) WriteHeader(statusCode int) {
	rw.statusCode = statusCode
	rw.ResponseWriter.WriteHeader(statusCode)
}

func (rw *responseWriter) Write(b []byte) (int, error) {
	n, err := rw.ResponseWriter.Write(b)
	rw.contentLength += n
	return n, err
}

func (h *Handler) Cache(w http.ResponseWriter, r *http.Request) {
	util.ResponseOk(w, h.cache.String())
}

func (h *Handler) CallApi(w http.ResponseWriter, r *http.Request) {
	var err error
	var urlParam = r.FormValue("url")

	token, err := util.GetTokenFromHeader(r)
	if err != nil {
		logger.Error(err)
		util.ResponseError(w, http.StatusUnauthorized, err.Error())
		return
	}

	var proxyReqHeaders = http.Header{}
	proxyReqHeaders.Set("Authorization", token)

	proxyURL, err := url.Parse(urlParam)
	if err != nil {
		logger.Error(err)
		util.ResponseError(w, http.StatusBadRequest, err.Error())
		return
	}

	proxyRequest := &http.Request{
		Method: http.MethodGet,
		URL:    proxyURL,
		Header: proxyReqHeaders,
		Body:   ioutil.NopCloser(nil),
	}

	var rw = &responseWriter{ResponseWriter: w}
	var proxy = httputil.NewSingleHostReverseProxy(proxyURL)
	proxy.ModifyResponse = func(resp *http.Response) error {
		return nil
	}

	proxy.ServeHTTP(rw, proxyRequest)

	var withTime, _ = ksuid.NewRandomWithTime(time.Now())
	var res = domain.Response{
		ID:     withTime.String(),
		Status: rw.statusCode,
		Length: int64(rw.contentLength),
	}

	var req = domain.Request{
		Token:  token,
		Method: http.MethodGet,
		URL:    urlParam,
	}

	h.cache.SetHTTPRequest(&req)
	h.cache.SetHTTPResponse(&res)

	util.ResponseOk(w, res)
}
