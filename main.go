package main

import (
	"context"
	"fmt"
	"kmf/proxy-transport-service/internal/cache"
	"kmf/proxy-transport-service/internal/handlers"
	"kmf/proxy-transport-service/internal/server"
	"kmf/proxy-transport-service/internal/service"
	"kmf/proxy-transport-service/pkg/logger"
	"os"
	"os/signal"
	"syscall"
)

// init logger
func init() {
	logger.SetLogger(logger.NewZapLogger())
}

func main() {
	var c = cache.NewCache()
	var srv = service.NewService()
	var h = handlers.NewHandler(srv, c)
	var s = server.NewServer(h)

	go s.Run(context.TODO())

	var quit = make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	var osSignal = <-quit

	logger.Info(fmt.Sprintf("program shutdown... call_type: %v", osSignal))
}
